# Python: 2.7

import utils as u
import gc
import argparse
import timeit
import sys

gc.collect()
start = timeit.default_timer()


def main(path, zip_output, date_output, print_count=0):
    """
    This is the main function. The input is taken from the "path" and the outputs are saved into two files,
    containing median values by dates and zip codes respectively.

    :param path: The input file path
    :param zip_output: The output file path for the median values by zip
    :param date_output: The output file path for the median values by zip
    :param print_count: Show counts for total donations, and valid donations (i.e. relevant to this study)

    NOTE: in this function u.medians, u.foe and u.no_fields are date structure for holding the medians,
    the fields we are interested in from the input file and the expected number of fields in each line 
    in the input file - respectively. 
    """

    """
    First the input file is validated to make sure that it contains at least one line of valid input fields.
    """
    if u.validate_lines_in_input_file(path):
        """
        Initializing counters to keep track of the given data, which may be relevant to the user.
        Will be shown only if print_count is greater than 0
        """
        total_donations = 0
        valid_individual_donations = 0
        valid_zip = 0
        valid_date = 0

        """
        Opening the zip file in a write mode to create an empty file, irrespective of its existence. 
        """
        u.open_file(zip_output)

        """
        Reading the input file one line at a time, keeping the processing of very large files possible.
        Then splitting the lines into separate fields. Then only considering the fields we are interested
        in and validating the given conditions.
        """
        with open(path) as infile:
            for line in infile:
                fields = line.strip().split('|')
                if len(fields) == u.no_fields:
                    total_donations += 1
                    other = fields[u.foe['OTHER_ID']].strip()
                    if other == "":
                        """
                        Only considering lines which have an empty OTHER_ID field.
                        """
                        amt = fields[u.foe['TRANSACTION_AMT']].strip()
                        if u.is_float(amt):
                            """
                            Only considering valid amounts (i.e. not empty).
                            """
                            cmte = fields[u.foe['CMTE_ID']].strip()
                            if cmte != "":
                                """
                                Only considering valid CMTE_ID. 
                                """
                                valid_individual_donations += 1
                                zip_val = fields[u.foe['ZIP_CODE']].strip()
                                if u.is_valid_zip(zip_val):
                                    """
                                    If the zip code is valid:
                                    1. the values are saved into a dictionary (data structure) which is
                                    used to calculate numbers, sums and running medians of the donations.
                                    2. the line is saved into the file containing medians. For better 
                                    performance this writing to the file is done in batches, rather than 
                                    one by one.
                                    """

                                    """
                                    Here, only considering first 5 digits of the zip codes.
                                    """
                                    zip_val = u.get_zip(zip_val)

                                    """
                                    Saving to the dictionary
                                    """
                                    (sum_val, median_val, length) = u.add_by('zip', cmte, zip_val, amt)

                                    """
                                    Writing to the file in batches
                                    """
                                    u.write_lines_by_batch(zip_output, cmte, zip_val, median_val, length, sum_val)

                                    valid_zip += 1
                                date = fields[u.foe['TRANSACTION_DT']].strip()
                                if u.is_valid_date(date):
                                    """
                                    If the date is valid, the values are saved into a dictionary (data structure),
                                    which is used to calculate numbers, sums and medians of the donations. 
                                    """
                                    u.add_by('date', cmte, date, amt)

                                    valid_date += 1

        if int(print_count) > 0:
            print("Total Donations: " + str(total_donations))
            print("Valid Individual Donations: " + str(valid_individual_donations))
            print("Valid Individual Donations with valid Zip Codes: " + str(valid_zip))
            print("Valid Individual Donations with valid Dates: " + str(valid_date))

        """
        Making sure all the lines from the array containing the lines are saved to the file.
        """
        u.write_lines(zip_output)

        """
        The median values grouped by dates are saved to the corresponding output file now.
        """
        u.write_file(u.medians['date'], date_output)

# print len(sys.argv)
# if len(sys.argv) > 1:
#     main(sys.argv[1], sys.argv[2], sys.argv[3])
# else:
parser = argparse.ArgumentParser(description='Find Political Donors')
parser.add_argument('--source', '-s', help='Source File', default='../input/itcont.txt')
parser.add_argument('--outzip', '-z', help='Output file for zip', default='../output/medianvals_by_zip.txt')
parser.add_argument('--outdate', '-d', help='Output file for date', default='../output/medianvals_by_date.txt')
parser.add_argument('--print_counts', '-p', help='Print counts', default='0')
args = parser.parse_args()
main(args.source, args.outzip, args.outdate, args.print_counts)

stop = timeit.default_timer()
print("Total runtime in seconds: ", stop - start)

gc.collect()
