import re
from math import modf
from numpy import median, sum

"""
Expected number of fields in each line of the input document.
"""
no_fields = 21

"""
Fields of interest according to the documentation and their indexes in each line. 
"""
foe = {
    'CMTE_ID': 0,
    'ZIP_CODE': 10,
    'TRANSACTION_DT': 13,
    'TRANSACTION_AMT': 14,
    'OTHER_ID': 15
}

"""
This dictionary will be used to calculate median and total amounts
"""
medians = {'zip': {}, 'date': {}}

"""
Batch size. This is selected after doing a performance testing.
More details in the util-test-performance.py file.
"""
batch_size = 100000

"""
This array works like a buffer containing the lines to write in a batch.
"""
lines_to_write = []


def add_by(name, cmte, field_value, amt):
    """
    This function adds values to the dictionary for median values for both the zip codes and the dates.
    :param name: either 'zip' or 'date' matching the initial definition of the "medians" dictionary.
    :param cmte: the receiver of the donation, gotten directly from the input file.
    :param field_value: the value of the zip code or date, assuming that they are in correct formats.
    :param amt: the amount of donation, gotten directly from the input file.
    :return: tuple of running sum, running median, and running number of donations for the
    corresponding 'cmte' and 'field_value'.
    """
    amt = float(amt)
    if cmte not in medians[name]:
        medians[name][cmte] = {field_value: []}
    elif field_value not in medians[name][cmte]:
        medians[name][cmte][field_value] = []
    medians[name][cmte][field_value].append(amt)
    amounts = medians[name][cmte][field_value]
    return format_float(sum(amounts)), round_float(median(amounts)), len(amounts)


def get_zip(zip_val):
    """
    Returns the first 5 digits of a valid  
    """
    return zip_val[0:5]


def validate_lines_in_input_file(path):
    """
    Validates that the input file from the given path contains at least one line containing
    valid fields of interest. 
    """
    with open(path) as infile:
        for line in infile:
            fields = line.strip().split('|')
            if len(fields) == no_fields:
                other = fields[foe['OTHER_ID']].strip()
                zip_val = fields[foe['ZIP_CODE']].strip()
                date = fields[foe['TRANSACTION_DT']].strip()
                amt = fields[foe['TRANSACTION_AMT']].strip()
                cmte = fields[foe['CMTE_ID']].strip()
                if other == "" and cmte != "" and is_float(amt) and (is_valid_zip(zip_val) or is_valid_date(date)):
                    return True
    return False


def is_float(text):
    """
    Validates whether the input text can be converted to a float number or not.
     
    Unit tested.
    """
    if re.match("^\d+?(\.\d+)?$", text) is None:
        return False
    else:
        return True


def is_valid_zip(text):
    """
    Validates whether the input text is a valid zip code or not.
    
    Unit tested.
    """
    if re.match("^\d{5}(\d+)?$", text) is None:
        return False
    else:
        return True


def is_valid_date(text):
    """
    Validates whether the input text is a valid date ot not. 
    
    Unit tested.
    """
    if re.match("^\d{8}$", text) is None:
        return False
    else:
        return True


def round_float(f):
    """
    Rounds the input to a nearest integer. Anything below .50 is dropped and anything 
    from $.50 and up are rounded to the next integer.
    
    Unit tested.
    """
    fraction, whole = modf(f)
    if fraction >= 0.5:
        return int(whole) + 1
    else:
        return int(whole)


def format_float(f):
    """
    Floating point numbers are formatted to not show unnecessary zeros after decimal point, if any.  
    
    Unit tested.
    """
    fraction, _ = modf(f)
    if fraction > 0:
        return f
    else:
        return int(f)


def open_file(path):
    """
    Opens file from the path in a write mode.  
    """
    zip_file = open(path, "w")
    return zip_file


def write_lines_by_batch(output, cmte, name, median_val, length, sum_val, batch=batch_size):
    """
    This function saves lines to the lines_to_write array. When the size of the array reaches to a
    certain batch size, the lines are written to the target output file.

    This function is created for performance testing with different batch sizes.
    """
    global lines_to_write
    lines_to_write.append("%s|%s|%s|%s|%s\n" % (cmte, name, str(median_val), str(length), str(sum_val)))
    if len(lines_to_write) >= batch:
        write_lines(output)


def write_lines_by_line(output, cmte, name, median_val, length, sum_val):
    """
    This function saves line by line.
    """
    file_object = open(output, "a")
    file_object.write(cmte + "|" + name + "|" + str(median_val) + "|" + str(length) + "|" + str(sum_val) + "\n")
    file_object.close()


def write_lines(output):
    """
    This function writes lines in the lines_to_write array into the output file. 
    """
    global lines_to_write
    file_object = open(output, "a")
    file_object.writelines(lines_to_write)
    file_object.close()
    lines_to_write = []


def write_file(date_med, date_output):
    """
    This function writes the dictionary containing donation amounts of donations grouped by 
    the receivers and the dates to a file, where the file is sorted by the receiver ids 
    and then dates.  
    """
    file_object = open(date_output, "w")
    for cmte, date_value in sorted(date_med.iteritems()):
        for date, value in sorted(date_value.iteritems()):
            median_val = round_float(median(value))
            length = len(value)
            sum_val = format_float(sum(value))
            file_object.write("%s|%s|%s|%s|%s\n" % (cmte, date, str(median_val), str(length), str(sum_val)))
    if not file_object.closed :
        file_object.close()

