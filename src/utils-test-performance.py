import timeit
import utils as u
import gc


def performance_write_line_batch():
    """
    This function is used to find a optimum batch size. Different batch 
    sizes are used for writing 1,000,000 lines in a file. 
    For a sample run, I got the following results where the tuple represents 
    a batch size and the corresponding runtime (in sec); and the system had
    16GB of memory, a processor with 8 cores and the OS was Debian based:

    (1000, 8.305593967437744)
    (10000, 8.103352785110474)
    (50000, 8.204941987991333)
    (100000, 8.20531702041626)
    (500000, 8.210314989089966)
    (1000000, 8.206636905670166)
    (0, 8.070548057556152)
    (1, 57.72929811477661)
    (-1, 49.61843490600586)

    [Here 0 corresponds to batch size of 10,000;
    -1 corresponds to writing line by line (one at a time)]
    I selected 10,000 to be the batch size, as it has a good performance
    and will not overload the memory.
    """
    for b in [1000, 10000, 50000, 100000, 500000, 1000000, 0, 1, -1]:
        gc.collect()
        start = timeit.default_timer()
        u.open_file("./write_line_test.txt")
        for _ in range(0, 10000000):
            if b == 0:
                u.write_lines_by_batch("./write_line_test.txt", "cmte", "12345", "123", "12", "1234")
            elif b == -1:
                u.write_lines_by_line("./write_line_test.txt", "cmte", "12345", "123", "12", "1234")
            else:
                u.write_lines_by_batch("./write_line_test.txt", "cmte", "12345", "123", "12", "1234", b)
        stop = timeit.default_timer()
        print(b, stop - start)
    gc.collect()

performance_write_line_batch()
