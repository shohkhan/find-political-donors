import unittest
import utils as u


class TestUtilMethods(unittest.TestCase):

    def test_round_float(self):
        """testing utils.round_float"""
        self.assertEqual(u.round_float(10.0), 10)
        self.assertEqual(u.round_float(10.4), 10)
        self.assertEqual(u.round_float(10.49), 10)
        self.assertEqual(u.round_float(10.50), 11)
        self.assertEqual(u.round_float(10.51), 11)
        self.assertEqual(u.round_float(10.60), 11)
        self.assertEqual(u.round_float(10.99), 11)

    def test_format_float(self):
        """testing utils.format_float"""
        self.assertEqual(u.format_float(10.0), 10)
        self.assertEqual(u.format_float(10.4), 10.4)
        self.assertEqual(u.format_float(10.49), 10.49)
        self.assertEqual(u.format_float(10.50), 10.50)
        self.assertEqual(u.format_float(10.99), 10.99)
        self.assertEqual(u.format_float(11), 11)
        self.assertEqual(u.format_float(11.0), 11)

    def test_is_valid_date(self):
        """testing utils.is_valid_date"""
        self.assertTrue(u.is_valid_date("01312017"))
        self.assertTrue(u.is_valid_date("01012016"))
        self.assertTrue(u.is_valid_date("12252015"))
        self.assertTrue(u.is_valid_date("09152018"))

        self.assertFalse(u.is_valid_date("25122015"))   # Only the mmddyyyy format is accepted
        self.assertFalse(u.is_valid_date("01312014"))   # Only years 2015-2019 are valid
        self.assertFalse(u.is_valid_date("00312017"))   # Malformed
        self.assertFalse(u.is_valid_date("013117"))     # Malformed
        self.assertFalse(u.is_valid_date("013120177"))  # Malformed
        self.assertFalse(u.is_valid_date(""))           # Empty

    def test_is_valid_zip(self):
        """testing utils.is_valid_zip"""
        self.assertFalse(u.is_valid_zip(""))
        self.assertFalse(u.is_valid_zip("0"))
        self.assertFalse(u.is_valid_zip("9123"))
        self.assertTrue(u.is_valid_zip("91234567"))
        self.assertTrue(u.is_valid_zip("91234"))

    def test_is_float(self):
        """testing utils.is_float"""
        self.assertFalse(u.is_float(""))
        self.assertTrue(u.is_float("0"))
        self.assertTrue(u.is_float("0.1"))
        self.assertTrue(u.is_float("10.25"))

if __name__ == '__main__':
    unittest.main()
