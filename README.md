## Readme

This program takes an input and creates two outputs, who by default have the following names:

1. `medianvals_by_zip.txt`: which contains a calculated running median, total dollar amount and total number of contributions by recipient and zip code.

2. `medianvals_by_date.txt`: which has the calculated median, total dollar amount and total number of contributions by recipient and date.

The code can be run by running the `run.sh` from shell. This file contains the shell command to run the python code which has the following format:

`python ./src/find_political_donors.py -s [input_file] -z [output_1] -d [output_2] -p [flag]`

Here, if the flag is set to grater than 0, the program will show details including the total number of donations, individual donations with valid amount and valid CMTE_ID, and further valid zip codes and valid dates of the provided input file; and total run time.

An example of running the code to show these details can be found in the `run_with_details.sh` file.

### Notes:
- Language used: Python 2.7.
- Libraries used: numpy, math, re, gc, argparse, timeit and unittest.
- For dates, only years from 2015 to 2019 are considered.
- To calculate medians, the median() function from numpy has been used.