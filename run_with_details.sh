#!/bin/bash
#
# Use this shell script to execute the code and see some details (-p is set to 1).
# This is an example, which can be modified as required.
#
python ./src/find_political_donors.py -s ./input/itcont.txt -z ./output/medianvals_by_zip.txt -d ./output/medianvals_by_date.txt -p 1